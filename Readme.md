# svFCS_Fluctuations

svFCS_Fluctuations implents a simulator of (sv)FCS experiments for protein diffusion + binding at the plasma membrane. This is the code used for the following paper: 

Anita Mouttou, Erwan Bremaud, Rayane Dibsy, Coline Arone, Julien Noero, Johson Mak, Delphine Muriaux, Hugues
Berry, and Cyril Favard (2022) Quantifying membrane binding using Fluorescence Correlation Spectroscopy diffusion laws

The current fork outputs the time-series of the fluorescence count, whereas the version of the original paper (at https://gitlab.inria.fr/hberry/gag_svfcs) outputs its auto-correlation function directly

*Usage*

- the C++ code must first be compiled using :

g++ -g svfcs_sim.cpp Particle.cpp -o svfcs_sim -O2 -std=c++11

- Once svfcs_sim is compiled:

	- all the parameters can be set in the python file launch.py. 
	Indicating vector values for parameters e.g., param1=[1 13 2], will launch one job per parameter value (e.g. one job for param1=1, one job for param1=13 and one job for param1=2). Beware of the combinatorics: param1=[1 3 5]; param2=[2 3 5 1 3], for instance, will launch 3x5= 15 jobs

	- execute launch.py with:
	python3.X launch.py

	This will automatically launch all the specified jobs

- File output:
	+ all files are saved in directories "FCS_D2XX_D3YYY" where XX and YYY are the values of the diff coeff in 2D (bound) and 3D (free),respectively. Warning: this directory is created by the code, so it will bug if the target directory allready exists (to avoid overwriting). Each directory contains:
		+ output.log: a redirection of the standard output. Mostly tells the jobs that are finished (replicates)
		+ xxx_yyy_zzz_fluctuations.json where xxx is the value of pon, yyy that of poff and zzz that of wxy: 
 		the file that contains the fluctuations of the fluoresence function corresponding to each of the parameter sets indicated in launch.py
		ie 1 row = 1 run, the value at column i of row i gives the number of photons received at time i delta_t of experiment j (delta_t is a parameter set in launch.py). 
		Warning: decimal separator (e.g. the dot .) is usually replaced by a "p" in the name of the fluctuations.json files








