/* svfcs_sim.cpp is the C++ main for the (sv)FCS simulator.

 Inputs: The input arguments are explicited line 75-124
 For simplicity, the file launch.py sets these input parameters
 and pass them to ./svfcs_sim after compilation (svfcs_sim.cpp must be compiled beforehand)
        
 Ouputs:  2 files:
 - output.log: the file where the standard output is redirected. Mostly tells the jobs that are finished (replicates)
 - xxx_yyy_zzz_fluctuations.json where xxx is the value of pon, yyy that of poff and zzz that of wxy: 
 the file that contains the fluorescence fluctuations corresponding to the parameters. 
 

 AUTHOR: Hugues Berry
         hugues.berry@inria.fr
         http://www.inrialpes.fr/Berry/

REFERENCE: Mouttou et al. Quantifying 2022 membrane binding using Fluorescence Correlation Spectroscopy diffusion laws

 LICENSE: CC0 1.0 Universal
*/



#include <iostream>
#include <random>
#include <fstream>
#include <iomanip>
#include "Particle.h"
#include <cstdlib>
#include <vector>
#include <math.h>
#include <cstring>
#include <ctime>
#include <string>
#include <map>
#include <utility>
#include <array>
using namespace std;

//the two following functions are used in the autocorrelation computation
// both inputs are: array_for_which_you_want_the_mean/var, array_size

/// WARNING : in the code, the cubic nucleus extends from -L/2 to +L/2 in each direction,
// and not from 0 to L, as in the paper
//so the plasma membrane in the code is located at z=-L/2 and not as z=0 as in the paper
// the paper convention is easier to explain whereas the code convention is more
// practical in terms of programming



int main(int argc, char **argv){
	

	int nb_simulations; // number of simulation for each wxy
	int n_part; //number of particle
	int mu; //number of photons emitted by each particle
	double delta_t; //the time step of the simulation (in sec)
	float t_end; //measurement time length
	float t_min; //initial time of the simulation (set to zero, usefull at the creation of the algorithm, maybe no longer usefull)


	float p_on_gag_mb; //gag to mb binding probability
	float p_off_gag_mb; 
	float wxy; //radius of the cylinder of light
	float wz; //high of cylinder of light
	float volume; //cube edge length of the simulation volume

	// diffusion parameters
	float D2; //diffusion constant of bound gag (micrometer²/sec)
	float D3; //diffusion constant of unbound gag (micrometer²/sec)
	float interaction_zone; //maximal length (in micromter) for gag to bind to the mb
	string name; //name of the current simulation depending on the given parameters (p_binding, p_unbinding and wxy)
	string dirname;//directory into which the simulations results must be saved
    
    /////////////BRILLANCE ////////////
    float brillance=15e4;//gives an average brillance of 2.3e3 - on average on molecules distributed at random in a volume of {-2*w_xy,2*w_xy} in x and y and {0,4*w_z} in z 

	// po::notify(vm);
    dirname=argv[1];
    name=argv[2];
    n_part=atoi(argv[3]);
    nb_simulations=atoi(argv[4]);
    wxy=atof(argv[5]);
    wz=atof(argv[6]);
    t_min=atof(argv[7]);
    t_end=atof(argv[8]);
    delta_t=atof(argv[9]);
    D3=atof(argv[10]);
    D2=atof(argv[11]);
    volume=atof(argv[12]);
    p_on_gag_mb=atof(argv[13]);
    p_off_gag_mb=atof(argv[14]);
    interaction_zone=atof(argv[15]);
    mu=atoi(argv[16]);

    
    name=dirname+"/"+name;

	//output files
	ofstream fluctuations_ostream(name + "_"  + "fluctuations.json");  //contains the fluorescence fluctuations of each simulation run (1 line = 1 simulation)
	

// compute some parameters
	const int time_size = (t_end / delta_t); //compute the number of time steps of the simulation

	float diffusion_2d = sqrt(delta_t * D2 * 2);//compute the 2D movement constant
	float diffusion_3d = sqrt(delta_t * D3 * 2);//same in 3D

	
	const float half_width = volume / 2;//compute the half of the edge of the simulation volume (one will use it later to randomly initialize the position of gags
	
	// initializing random generators
	std::mt19937_64 generator(std::time(nullptr));

	normal_distribution<float> distribution(0.0, 1.0); // this ditribution generate normally distributed reals (mean=0.0, sigma=1.0) (outputs are used to compute the Random Walk (RW) of each particle in each dimension)
	uniform_real_distribution<float> real_distrib(-half_width, half_width); //this distribution is used to initialize gags initial random positions
	uniform_real_distribution<float> distrib_0_1(0, 1); //generate real number between 0 and 1. used to draw probabilities


	//non-dynamically initialize an array which will contain the fluctuations of the simulations
	float** fluorescence = new float*[nb_simulations];
	for (int i=0;i<nb_simulations;i++){
		fluorescence[i]=new float[time_size];
	}
	
//SIMULATION START
	// iterate over the number of replicates (with the same wxy)
	for (int k = 0; k < nb_simulations; k++){
		std::cout << "wxy = "<<wxy<<" starts replicate " << k << std::endl;

		// creating n_part gags randomly located and put them in a vector
		vector <Particle>  obj_vect;
		obj_vect.reserve(n_part);
		for (int i=0; i < n_part; i++){
			Particle gag(i , real_distrib(generator), real_distrib(generator), real_distrib(generator), diffusion_2d, diffusion_3d, p_on_gag_mb, p_off_gag_mb, wz, wxy, interaction_zone, volume, brillance);
			obj_vect.push_back(gag);

		}
		
		//let the system reach equilibrium during 10 second, without mesuring any parameters
        //First, prepare the system close to the theoretical equilibrium by binding a fraction kon/(kon+koff) of the molecules to the membrane
        float kon=p_on_gag_mb/volume*interaction_zone;
        int n_Beq=(int)(kon/(kon+p_off_gag_mb)*n_part);
        

        for (int p = 0; p < n_Beq; p++) {
            obj_vect[p].position[0]= -half_width + 0.5*interaction_zone;//position[0]=z-coordinate
            obj_vect[p].binding_state =true;
        }
        //then let the system equilibrate further for 500 msec
		for (double j = 0; j < 0.5; j += delta_t){
			//iterate on gags to make them move/bind..
			for (int i = 0; i < n_part; i++){

				// free RW
                obj_vect[i].displacement(distribution(generator), distribution(generator), distribution(generator));
                
                //check if the location of the particle is not ouside of the volume
                //if it is, a rebound is computed for the bottom z-boundary (membrane)
                // on the upper z-boundary the molecule is relocated in a possible binding position
                //for the other boundary, the molecule is relocated at random close to another x- or y-boundary
                
                // TO DO: put the boundary conditions below in a function so as to call it back after the equilibration
                // instead of copy-pasted code

                if (obj_vect[i].position[0] < - half_width) {// adhesion at the upper z-boundary ie the membrane
                    obj_vect[i].position[0] = -half_width + 0.5*interaction_zone;
                }
                else if (obj_vect[i].position[0] > half_width) {// rebound at the bottom z-boundary
                   obj_vect[i].position[0] = volume - obj_vect[i].position[0];
                }
                if ((obj_vect[i].position[1] < - half_width)||(obj_vect[i].position[1] > half_width) ||(obj_vect[i].position[2] < - half_width)||(obj_vect[i].position[2] > half_width)){
                    //first choose by which of the 5 faces of the volume the molecule will be reintegrated
                    float unif01=distrib_0_1(generator);
                    if (unif01<0.25)
                    {//re-enter though x=-half-width
                        obj_vect[i].position[1]=-half_width+interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[2]=real_distrib(generator);
                    }
                    else if (unif01<0.50)
                    {//re-enter though x=+half-width
                        obj_vect[i].position[1]=half_width-interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[2]=real_distrib(generator);
                    }
                    else if (unif01<0.75)
                    {//re-enter though y=-half-width
                        obj_vect[i].position[2]=-half_width+interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[1]=real_distrib(generator);
                    }
                    else
                    {//re-enter though y=half-width
                        obj_vect[i].position[2]=half_width-interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[1]=real_distrib(generator);
                    }

                }
                
				obj_vect[i].check_binding(distrib_0_1(generator)); //compute the current binding state
			}//loop on molecules
            
            
		}//loop on times for equlibration
		
		int time_counter = 0; //this parameter increase by one at each time step to save fluorescence at the good position in the fluorescence array

		//start mesuring the parameters
		for (double j = t_min; j < t_end; j += delta_t){
			for (int i = 0; i < n_part; i++){
				// compare random to the fluorescence emitted by gag, if random< proba, mu photons are added to the fluorescence array
				float r = distrib_0_1(generator);
				float fluo = delta_t*obj_vect[i].photon_emission();
  

				if ( (fluo) >= r) {
					fluorescence[k][time_counter] += mu;
				}


                // free RW
                obj_vect[i].displacement(distribution(generator), distribution(generator), distribution(generator));
                
                //check if the location of the particle is not ouside of the volume
                //if it is, a rebound is computed for the bottom z-boundary (membrane)
                // on the upper z-boundary the molecule is relocated in a possible binding position
                //for the other boundary, the molecule is relocated at random close to another x- or y-boundary
                
                if (obj_vect[i].position[0] < - half_width) {// adhesion at the upper z-boundary ie the membrane
                    obj_vect[i].position[0] = -half_width + 0.5*interaction_zone;
                }
                else if (obj_vect[i].position[0] > half_width) {// rebound at the bottom z-boundary
                        obj_vect[i].position[0] = volume - obj_vect[i].position[0];
                    }
                if ((obj_vect[i].position[1] < - half_width)||(obj_vect[i].position[1] > half_width) ||(obj_vect[i].position[2] < - half_width)||(obj_vect[i].position[2] > half_width)){
                    //first choose by which of the 5 faces of the volume the molecule will be reintegrated
                    float unif01=distrib_0_1(generator);
                    if (unif01<0.25)
                    {//re-enter though x=-half-width
                        obj_vect[i].position[1]=-half_width+interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[2]=real_distrib(generator);
                    }
                    else if (unif01<0.50)
                    {//re-enter though x=+half-width
                        obj_vect[i].position[1]=half_width-interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[2]=real_distrib(generator);
                    }
                    else if (unif01<0.75)
                    {//re-enter though y=-half-width
                        obj_vect[i].position[2]=-half_width+interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[1]=real_distrib(generator);
                    }
                    else
                    {//re-enter though y=half-width
                        obj_vect[i].position[2]=half_width-interaction_zone;
                        obj_vect[i].position[0]=real_distrib(generator);
                        obj_vect[i].position[1]=real_distrib(generator);
                    }
                    
                }
                obj_vect[i].check_binding(distrib_0_1(generator)); //compute the current binding state
                
            }//loop on molecules

			time_counter += 1;
			
		}//loop on times
		
		std::cout << "wxy = "<<wxy<<" finished replicate " << k <<std::endl;
        
	}//loop on runs

	//save the fluorenscence fluctuation array and print it in fluctuations.csv
	for (int k = 0; k < nb_simulations; k++) {
  		for (int i = 0; i < time_size; i++)
    		fluctuations_ostream << fluorescence[k][i] <<',';
  		fluctuations_ostream << '\n';
	}

	//delete the pointer
	delete [] fluorescence;

	return 0;
}

//COMPILE WITH:
//g++ -g svfcs_sim.cpp Particle.cpp -o svfcs_sim -O2 -std=c++11