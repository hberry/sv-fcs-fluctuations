#written for python 3.9

# launch.py is the configuration file where the parameters of the FCS simulator are set.
#
# launch a job for each value of the parameters p_on, p_off, wxy indicated 
# and pass all the parameters in option to the compiled gag_sim 
# i.e. as options to ./svfcs_sim - see line 149
#
# Output: save all parameters in a json file svfcs_parameters.json
#        
# AUTHOR: Hugues Berry
#         hugues.berry@inria.fr
#         http://www.inrialpes.fr/Berry/
#
# REFERENCE: Mouttou et al. Quantifying membrane binding using Fluorescence Correlation Spectroscopy diffusion laws
#
# LICENSE: CC0 1.0 Universal




from __future__ import division
import subprocess
import time
import numpy as np
import json
import os

#This script launches automatically simulations with parameters given below.
#It also saves all the parameters in a json file ("svfcs_parameters.json"), which will be 
#loaded by every results analysis scripts to open the simulation output files,
#analyse and represent them

np.set_printoptions(precision=2)

from numpy.random import default_rng
rng = default_rng()


#Illumination volumes:
#wxy and wz are the parameters of the gaussian illumination profile in the three dimensions. 
#omega is the ration wz/wxy (fixed here) 
omega = 5

#standard values for wxy in micrometers:
wxy_vector = [0.071,0.132]
#[0.071,0.132, 0.173,0.241,0.295,0.340,0.379,0.415,0.448,0.479,0.508,0.535,0.561,0.586,0.610,0.632];


#and the corresponding value of wz
wz_vector = [wxy * omega for wxy in wxy_vector] 

# the diffusing and binding protein is called "gag" all along the code
number_of_gag = 100

number_of_simulations = 20

#this parameter represent the number of autocorrelation profiles meaned
# by set of parameters (k_on, k_off and wxy)

t_min = 0.0
#beginning of the simulation

t_end = 11
#end of the simulation (in sec)

delta_t = 1e-6 
#time_step of the simulation

D3_vector=np.array([25]).astype(np.int32)#rng.uniform(25,45,1).astype(np.int32)

D3 = D3_vector[0]
#3D diffusing gag diffusion constant in micrometers^2/sec

D2 =np.around(rng.uniform(1.5,5,1),decimals=2)[0]
#2D diffusing gag diffusion constant in micrometers^2/sec


mu = 1 
#number of photons emitted by a gag (this value can be 1 or 2, depending on the simulated experience)


vol = 6
#initial volume edge of the simulation in micrometers



interaction_zone = 0.01 
#max distance (in micrometer) for which a gag can bind the membrane
#called epsilon in the article


KD_vector=rng.uniform(1,10,1)

pon_vector=rng.uniform(0.005,0.1,1)

poff_vector=np.multiply(KD_vector,interaction_zone/vol*pon_vector)



pool = []

#now, launch a job for each set of parameters p_on, p_off, wxy, and give all the parameters is option to the compiled gag_sim

dirname="FCS_D2{}_D3{}".format(D2, D3)
os.mkdir(dirname);

with open(dirname+"/"+"output.log", 'w') as f:
    for pon_index, pon in enumerate(pon_vector):
        for poff_index, poff in enumerate(poff_vector):
            for index, wxy in enumerate(wxy_vector):
                options = [dirname,
                   "{}_{}_{}".format(np.around(pon,6), np.around(poff,6), wxy), #the name of the output files of a specific simulation begins with its parameters(p_on,p_off and wxy) 
                   str(number_of_gag),
                   str(number_of_simulations),
                   str(wxy),
                   str(wz_vector[index]),
                   str(t_min),
                   str(t_end),
                   str(delta_t),
                   str(D3), 
                   str(D2), 
                   str(vol),
                   str(np.around(pon_vector[pon_index],6)),
                   str(np.around(poff_vector[poff_index],6)),
                   str(interaction_zone),
                   str(mu)] 
                proc = subprocess.Popen(["./svfcs_sim"] + options,stdout=f, stderr=f)
                pool.append(proc)
                time.sleep(3.0) # little pause to prevent a commun initialization of all the random generators of svfcs_sim



#ask the launch process to wait for child jobs to end before its own end
for process in pool:
    process.wait()

print('All processes done')

